import java.util.*

fun main() {
    // dav 1
    val numArray = intArrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9)
    println( "average : "+dav_1(numArray))

    // dav 2
    val word = "level"
    println( "Word is palindrome : "+dav_2(word))


}

fun dav_1(num: IntArray): Int {

    val oddArray = num.filter{ it % 2 != 0 }.toTypedArray()
    var sum = 0;
    for (i in oddArray) {
        sum += i
    }
    val average = sum / oddArray.size
    return average


}
fun dav_2(word: String): Boolean {
    val word_1 = word;
    val revers = word.reversed()
    return word_1 == revers
}
